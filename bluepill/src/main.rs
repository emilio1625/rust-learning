//! Blinks an LED
//!
//! This assumes that a LED is connected to pc13 as is the case on the blue pill board.
//!
//! Note: Without additional hardware, PC13 should not be used to drive an LED, see page 5.1.2 of
//! the reference manual for an explanation. This is not an issue on the blue pill.

#![deny(unsafe_code)]
#![no_std]
#![no_main]

use panic_itm as _;

// use nb::block;

use cortex_m_rt::entry;
// use cortex_m_semihosting::hprintln;
// use embedded_hal::digital::v2::OutputPin;
// use stm32f1xx_hal::{pac, prelude::*, timer::Timer};
use cortex_m::iprintln;
use stm32f1xx_hal as _;

#[entry]
fn main() -> ! {
    let cp = cortex_m::Peripherals::take().unwrap();
    let mut itm = cp.ITM;

    iprintln!(&mut itm.stim[0], "Hola mundo! :D");

    loop {}
}
